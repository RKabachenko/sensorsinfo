__author__ = 'Roman'
from DataModel.SensorDataRecord import SensorDataRecord


class SensorsDumpParser(object):
    def ParseFile(self, file_string):
        records_list = []

        strings = file_string.split("\n")
        for record_string in reversed(strings):
            tokens = record_string.split("|")
            if len(tokens) == 4:
                records_list.append(self.SensorsDataRecordFromString(tokens))

        return records_list

    def SensorsDataRecordFromString(self, record_string):
        data_record = SensorDataRecord()
        data_record.location = record_string[0]
        data_record.name = record_string[1]
        data_record.date = record_string[2]
        data_record.value = record_string[3]
        return data_record
