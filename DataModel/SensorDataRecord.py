__author__ = 'Roman'


class SensorDataRecord(object):
    location = None
    name = None
    date = None
    value = None

    def __init__(self):
        self.location = None
        self.name = None
        self.date = None
        self.value = None

    def serialize(self):
        return {
            'location': self.location,
            'name': self.name,
            'date': self.date,
            'value': self.value
        }