import codecs
import os
from flask import Flask, render_template, jsonify
from DataModel.SensorsDumpParser import SensorsDumpParser

data_records = None
dates = None
sensors = None
app = Flask(__name__)


def parseFile():
    global data_records
    global dates
    global sensors
    try:
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, "sensors_dump.txt"), "r") as sensors_dump_file:
            file_string = sensors_dump_file.read()
        if file_string is not None:
            parser = SensorsDumpParser()
            data_records = parser.ParseFile(file_string)
            dates = []
            sensors = []
            for data_record in data_records:
                date = data_record.date
                sensor_name = data_record.name
                if date not in dates:
                    dates.append(date)
                if sensor_name not in sensors:
                    sensors.append(sensor_name)
    except Exception as e:
        print(e.message)


@app.route('/')
def indexView():
    return render_template('index.html')


@app.route('/_getDataRecords/<sensor_name>')
def getDataRecords(sensor_name):
    try:
        global data_records
        sensor_name = sensor_name.encode('utf8')
        records_for_sensor = [data_record for data_record in data_records if data_record.name == sensor_name]
        return jsonify(results=[record.serialize() for record in records_for_sensor])
    except Exception as e:
        print(e.message)


@app.route('/_getDates')
def getDates():
    global dates
    return jsonify(results=dates)


@app.route('/_getSensors')
def getSensors():
    global sensors
    return jsonify(results=sensors)


if __name__ == '__main__':
    parseFile()
    app.run()

